import { Deck, Slide, Heading, CodePane, Text, UnorderedList, ListItem, Image, Grid }  from 'spectacle';
import jsRuntime from './js_runtime.png';

function App() {
  return (
    <Deck>
      <Slide>
        <Heading>JavaScript</Heading>
      </Slide>
      <Slide>
        <Heading textAlign="center">History</Heading>
        <UnorderedList>
          <ListItem>
            Mid 1990s browser wars and in September 1995 Brendan Eich developed
            a new scripting language in 10 days. A few naming changes later
            'javascript' stuck
          </ListItem>
          <ListItem>
            In 1997, the standards work was handed off to the ECMA. 1997 to 1999 ES1, 2 and 3 were released
          </ListItem>

          <ListItem>
            2003 ES4 - abandoned
          </ListItem>

          <ListItem>
            2009 December - ES5
          </ListItem>

          <ListItem>2011 - ES6</ListItem>

          <ListItem>Rest is history</ListItem>
        </UnorderedList>
      </Slide>

      <Slide>
        <Heading>Runtimes</Heading>
        <Grid gridTemplateColumns="1fr 1fr">
          <img src={jsRuntime} height="80%"></img>
          <UnorderedList>
            <ListItem>Node, Chrome - V8</ListItem>
            <ListItem>Firefox - SpiderMonkey</ListItem>
            <ListItem>Safari, React-Native, WebKit - JSCore</ListItem>
          </UnorderedList>
        </Grid>
      </Slide>

      <Slide>
        <Heading>Types & operators</Heading>
        <CodePane language="javascript" highlightRanges={[
          [1, 6],
          [10, 12],
          [14, 21],
          [23, 25],
          [27, 29]
        ]}>
          {`
            /** number */
            123
            123.99
            123e8         // 123 * 10^8 (12300000000)
            123 * 2 + 1   // 247
            123.99 * .99  // 122.75009999999999

            /** special numbers */

            Infinity
            -Infinity
            NaN

            /** string */
            "Howdy!"
            'hello'
            "Hello, " + "world!"
            \`Result of 1 and 2 is \${1 + 3}\`

            "Line 1\\nLine 2 \\\\"   // Line 1
                                  // Line 2 \\

            /** boolean */
            true
            false

            /** null & undefined */
            undefined
            null
          `}
        </CodePane>
      </Slide>

      <Slide>
        <Heading>Operators</Heading>
        <CodePane language="js" highlightRanges={[
          [1, 2], [4, 5], [7, 11], [13, 15], [17, 21]
        ]}>
          {`
          12 + 2 * 2 > 33     // false
          (12 + (2 * 2)) > 33

          true || false       // true
          true && false       // false

          /** short circuiting */
          false || "abc"      // "abc"
          true || "abc"       // true
          true && "abc"       // "abc"
          false && "abc"      // false

          /** unary */
          !false              // true
          !true               // false

          /** ternary */
          true ? 123 : 321    // 123
          false ? 123 : 321   // 321

          const test = result > 0 ? result - 1 : result + 1
          `}
        </CodePane>
      </Slide>

      <Slide>
        <Heading>typeof</Heading>
        <CodePane language="js" highlightRanges={[
        ]}>
          {`
          typeof undefined  // 'undefined'
          typeof true       // 'boolean'
          typeof 123        // 'number'
          typeof "test"     // 'string'

          typeof null       // 'object'
          typeof NaN        // 'number'
          `}
        </CodePane>
      </Slide>

      <Slide>
        <Heading>Automatic type conversion</Heading>
        <CodePane language="js" highlightRanges={[
          [1, 5], 7, [9, 11], [12, 17]
        ]}>
          {`
          !undefined    // true
          !null         // true
          !123          // false
          !""           // true
          !"test"       // false

          12 * null         // 0

          "5" - 1           // 4
          "5" + 1           // '51'

          123 == "123"      // true
          false == 0        // true
          null == undefined // true
          NaN == NaN        // false

          123 === "123"     // false

          `}
        </CodePane>
      </Slide>

      <Slide>
        <Heading>Bindings</Heading>
        <CodePane language="js" highlightRanges={[
          [1, 2], [4, 5], [7, 8], [10, 14], [17, 23]
        ]}>
          {`
          const a = 3
          a = 4         // Uncaught TypeError: Assignment to constant variable.

          let b = 12 * 2
          b = 4         // 4

          var c = b ? "b is truthy" : "b is falsy"
          c = 6         // 6

          const a = 2;  // Uncaught SyntaxError: redeclaration of const a
          let b = 2;    // Uncaught SyntaxError: redeclaration of const b
          var c = 2;

          c             // 2


          if (true) {
            var testVar = 123
            let testLet = 321
          }

          testVar       // 123
          testLet       // Uncaught ReferenceError: testLet is not defined
          `}
        </CodePane>
      </Slide>

      <Slide>
        <Heading>Functions</Heading>
        <CodePane language="js" highlightRanges={[
          [1, 6], [8, 13], [15, 17], [23, 27], [29, 36]
        ]}>
          {`
          /** Functions */

          /** Standard */
          const func = function () {
            return "hello";
          }

          /** Arrow */
          const a = () => {}
          const b = () => { return "hello" }
          const c = () => "hello"

          typeof a // 'function'

          a() === undefined // true
          b() === c()       // true

          /** Declaration notation */
          function greet() {
            return "hello"
          }

          /** Parameters */

          const test = (a, b, c) => a + b * c
          test(1, 2)                // 1 + 2 * undefined  // NaN
          test(1, 2, 3, 4, 5)       // 1 + 2 * 3          // 6

          /** Immediately invoked */
          (() => {
            // do stuff
          })()

          (function() {
            // do stuff
          })()
          `}
        </CodePane>
      </Slide>

      <Slide>
        <Heading>Stack trace and errors</Heading>
        <Grid gridTemplateColumns="1fr">
          <img src={jsRuntime} height="80%"></img>
        </Grid>
      </Slide>

      <Slide>
        <Heading>Stack trace and errors</Heading>

        <CodePane language="js" highlightRanges={[
          [1, 7], 9, [11, 22], [23, 24]
        ]}>
          {`
          const a = () => {
            throw new Error("something bad")
          }

          function b(func) {
            func()
          }

          b(() => a());             // Uncaught Error: something bad + stack trace

          try {
            b(() => a())
          } catch (err) {
            err                     // { message: "something bad"
          }                         //   stack: "
                                    //    a
                                    //    anonymous
                                    //    b
                                    //    anonymous
                                    //  "
                                    // }

          a.name                    // "a"
          b.name                    // "b"
          `}
        </CodePane>
      </Slide>

      <Slide>
        <Heading>Function notes</Heading>
        <CodePane language="js" highlightRanges={[
          [1, 12], [14, 21]
        ]}>
          {`
          /** Declaration order */
          greet()       // 'hello'
          //...
          function greet() {
            return "hello";
          }

          greetAgain()  // Uncaught ReferenceError: greetAgain is not defined

          const greetAgain = () => {
            return "howdy";
          }

          /** Closures */

          const add = (first) => {
            return (second) => first + second
          }

          const add2 = add(2)
          const add3 = add(3)
          `}
        </CodePane>
      </Slide>


      <Slide>
        <Heading>Objects & Arrays</Heading>
        <CodePane language="js" highlightRanges={[
          [1, 6], [8, 14], [16, 18], [20, 29], 31
        ]}>
          {`
          /** Objects & arrays */
          const a = { hello: 'world' }
          const arr = ['foo', 'bar', 'baz']

          typeof a      // 'object'
          typeof arr    // also 'object'

          a.hello       // 'world'
          a['hello']    // 'world'

          a.whatever = 'new'
          a['whatever else'] = ['and', 'again']

          a             // { whatever: 'new', hello: 'world', 'whatever else': ['and', 'again'] }

          a.undef = undefined
          a.undef       // undefined
          a.nondef      // undefined

          /** Mutability */

          const b = a
          const c = b

          c.test = 'yes'

          a             // { ..., 'test': 'yes' }

          a === c       // true

          ({}) === ({}) // false
          `}
        </CodePane>
      </Slide>

      <Slide>
        <Heading>Spread sugar</Heading>
        <CodePane language="js" highlightRanges={[
          [1, 2], [4, 9], [10, 18], [21, 25]
        ]}>
          {`
          const name = 'test'
          const obj = { name, a: 33 }    // { name: 'test', a: 33 }

          const { a, ...restObj } = obj
          a                              // 33
          restObj                        // { name: 'test' }

          const [a1, a2, ...rest] = [1, 2, 3, 4]
          a1, a2, rest                   // 1, 2, [3, 4]

          ({ name }) => {
            name
          }

          ([a, b]) => {
            a
            b
          }


          const spreadObj = { a: 1, b: 2 }
          const spreadArr = [3, 4]

          { c: 3, ...spreadObj }         // { c: 3, a: 1, b: 2 }
          [1, 2, ...spreadArr]           // [1, 2, 3, 4]
          `}
        </CodePane>
      </Slide>

      <Slide>
        <Heading>Methods & prototypes</Heading>
        <CodePane language="js" highlightRanges={[
          1, [3, 9], [11, 18], [20, 25], [27, 30], [32, 36], [39, 49], [52, 60]
        ]}>
          {`
          someObj.method()

          /** Methods */
          const a = {
            thing: 'string',
            do: () => 3
          };

          a.do()                    // 3

          a.do = function() {
            return this.thing
          }

          a.do()                    // 'string'

          /** only works for non arrow functions */
          a.do = () => this.thing   // undefined

          const b = {}
          b.toString                // function toString()

          b.__proto__               // Object.prototype

          // ({}).toString, ({}).__proto__.toString, ({}).__proto__.__proto__.toString, ..., null

          ([]).__proto__            // Array.prototype
          ("string").__proto__      // String.prototype
          (() => {}).__proto__      // Function.prototype
          (123).__proto__           // Number.prototype

          const a = Object.create({
            meOwnThing: function() {
              // do things
            }
          })                        // a.meOwnThing // function


          function ClassLike(properties) {
            this.fields = properties
          }

          ClassLike.prototype.method = function(props) {
            this.fields * props;
          }

          const c = new ClassLike(123)
          c.fields                  // 123
          c.method(2)               // 246


          class ClassLike {
            constructor(properties) {
              this.fields = properties
            }

            method(props) {
              this.fields * props
            }
          }
          `}
        </CodePane>
      </Slide>

      <Slide>
        <Heading>Prototype notes</Heading>
        <CodePane language="js" highlightRanges={[[1, 10], [13, 20]]}>
          {`
          /** Function.call && Function.apply */
          const func = function() {
            return this.fields
          }

          const thisObj = { fields: 123 }

          func.call(thisObj, p1, p2, p2, ...)          // 123
          func.bind(thisObj)                           // function
          func.appy(thisObj, [p1, p2, p3, ...])


          oops                    // Uncaught ReferenceError: oops is not defined

          function noThis() {
            this.oops = 3
          }

          noThis()
          oops                    // 3
          `}
        </CodePane>
      </Slide>


      <Slide>
        <Heading>Async</Heading>

        <CodePane language="js" highlightRanges={[
          [1, 4], [6, 9], [11, 22], [24, 38], [40, 46]
        ]}>
          {`
          /** Callbacks */
          setTimeout(() => {
            console.log(123)
          }, 2000)

          function (args, callback) {
            // do stuff
            callback(results)
          }

          /** Promises */
          makePromise()
            .then((result) => {})
            .catch(err => {
              err                     // "timeout"
            })

          const makePromise = () => new Promise((resolve, reject) => {
            setTimeout(() => {
              reject("timeout")
            }, 2000)
          })

          fetch('url', (res) => {
            processResponse(res, (data) => {
              doSomethingElse(data, () => {
                response
              })
            })
          })

          fetch('url')
            .then((res) => processResponse(res))
            .then((data) => doSomethingElse(data))
            .then(() => response)     // undefined
            .catch(err => {
              err
            })

          /** async & await */
          async () => {
            let res = await fetch('url')
            let data = await processResponse(res)
            await doSomethingElse(data)
            response                  // still there
          }
          `}
        </CodePane>
      </Slide>

      <Slide>
        <Heading>Static analysis</Heading>
        <Text>Or anything you can do without actually running the code</Text>
        <UnorderedList>
          <ListItem>Linting</ListItem>
          <ListItem>Type checks</ListItem>
        </UnorderedList>
      </Slide>

      <Slide>
        <Heading>Linting</Heading>
        <Text>Main tool for javascript in this field is eslint</Text>
        <UnorderedList>
          <ListItem>External tool separate from the code</ListItem>
          <ListItem>Can catch simple issues and common patterns</ListItem>
          <ListItem>Ultimately used to enforce consistency</ListItem>
        </UnorderedList>
      </Slide>

      <Slide>
        <Heading>Type checking</Heading>
        <CodePane language="ts" highlightRanges={[]}>
          {`
          const a = { }

          /** In pure js */
          "Hello " + a.name       // 'Hello undefined'

          /** In typescript */
          "Hello " + a.name       // Property 'name' does not exist on type '{}'

          const b = "123"
          b * 123                 // The left-hand side of an arithmetic operation must be of type
                                  // 'any', 'number', 'bigint' or an enum type.
          `}
        </CodePane>
      </Slide>

      <Slide>
        <Heading>Tests</Heading>
        <Text>Anything we can do with actually running the code</Text>
        <UnorderedList>
          <ListItem>Mocha - older, more minimal test runner</ListItem>
          <ListItem>Jest - newer, fancier test runner + utility library</ListItem>
        </UnorderedList>
      </Slide>

      <Slide>
        <Heading>Mocha vs Jest</Heading>
        <CodePane language="js" highlightRanges={[10, 19]}>
          {`
          describe('test pack', () => {
            beforeEach(() => {
              // setup each test
            })

            it('some case name', () => {
              const a = 3;
              const b = 2

              /** Mocha requires additional library for this */
              expect(add(a, b)).toBe(a + b)

              /** Jest utilities */
              jest.fn()
              jest.useFakeTimers()

              /** Following works in jest with minor configuration */
              document.createElement('div')
              window.requestAnimationFrame
            })

            test('some other case name', () => {
              // ...
            })
          })
          `}
        </CodePane>
      </Slide>

      <Slide>
        <Heading>Questions</Heading>
      </Slide>
    </Deck>
  );
}

export default App;
